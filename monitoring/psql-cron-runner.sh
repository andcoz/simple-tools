#!/bin/sh
#
#  Sends by email the output of some postgresql queries
#
#  TODO insert a brief explanation here
#
#  Copyright  2013-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

## Parametri di connessione al db

PGHOST=
PGPORT=
PGDATABASE=somedatabase
PGUSER=someuser

# Per evitare, che venga chiesta la password è  
# necessario creare un file ~/.pgpass contenente
# una riga del tipo: 
#
# localhost:5432:*:someuser:somepassword
#
# dove i campi sono, nell'ordine:
#
# PGHOST:PGPORT:PGDATABASE:PGUSER:PASSWORD

SMTPSERVER='smtp.somedomain.local'

FROM='you@somedomain.local'
TO='me@somedomain.local'
SUBJECT="[$(hostname -a)] some_stats"

QUERIES=(
        "select now()"
	"select * from some_view"
	"select * from some_table where some_colum is null"
        "select now()"
)

# -- -+-_== -- -------------------------------------------------------

export PGHOST PGDATABASE PGPORT PGUSER

PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/maggioli.julia/bin

TMPFILE=$(mktemp)
CSVFILE=$(mktemp)

cat << EOF > "${TMPFILE}"
<html>
  <head>
    <style>
      /*
      --------------------------------------------------------------------------------
      What:	"Oranges in the sky" Styles(Table data design)
      Who:	Krasimir Makaveev(krasi [at] makaveev [dot] com)
      When:	15.09.2005(created)
      --------------------------------------------------------------------------------
      */
      
      table {
	      font-family: Verdana, Arial, Helvetica, sans-serif;
	      border-collapse: collapse;
	      border-left: 1px solid #ccc;
	      border-top: 1px solid #ccc; 
	      color: #333;
      }
      
      table caption {
	      font-size: 1.1em;
	      font-weight: bold;
	      letter-spacing: -1px;
	      margin-bottom: 10px;
	      padding: 5px;
	      background: #efefef;
	      border: 1px solid #ccc;
	      color: #666;
      }
      
      table a {
	      text-decoration: none;
	      border-bottom: 1px dotted #f60;
	      color: #f60;
	      font-weight: bold;
      }
      
      table a:hover {
	      text-decoration: none;
	      color: #fff;
	      background: #f60;
      }
      
      table tr th a {
	      color: #369;
	      border-bottom: 1px dotted #369;
      }
      
      table tr th a:hover {
	      color: #fff;
	      background: #369;
      }
      
      table thead tr th {
	      text-transform: uppercase;
	      background: #e2e2e2;
      }
      
      table tfoot tr th, table tfoot tr td {
	      text-transform: uppercase;
	      color: #000;
	      font-weight: bold;
      }
      
      table tfoot tr th {
	      width: 20%;
      }
      
      table tfoot tr td {
	      width: 80%;
      }
      
      table td, table th {
	      border-right: 1px solid #ccc;
	      border-bottom: 1px solid #ccc;
	      padding: 5px;
	      line-height: 1.8em;
	      font-size: 0.8em;
	      vertical-align: top;
	      width: 20%;
      }
      
      table tr.odd th, table tr.odd td {
	      background: #efefef;
      }
    </style>
  </head>
  <body>
EOF

for QUERY in "${!QUERIES[@]}"; do

     psql -H -c "\\copy ( ${QUERIES[$QUERY]} ) to ${CSVFILE} CSV HEADER;";

     # TODO note that character escaping is not well done.
     # Values containing a comma can break the output.
     awk -F '[,]' -f - -v caption="${QUERIES[$QUERY]}" "${CSVFILE}" >> "${TMPFILE}" <<EOF
BEGIN { 
        printf("<table><caption>%s</caption>", caption);
}
NR==1{
        printf("<thead><tr>");
        for( i = 1 ; i <= NF ; ++i ) printf("<th>%s</th>", \$i);
        printf("</tr></thead><tbody>");
}
NR>1{
        printf("<tr class=\"%s\">", NR%2 ? "even" : "odd" );
        for( i = 1 ; i <= NF ; ++i ) printf("<td>%s</td>", \$i);
        printf("</tr>");
}
END {
        printf("</tbody></table>\n");
} 
EOF
    echo "<hr/>" >> "${TMPFILE}" 

done;

echo "</body></html>" >> "${TMPFILE}"

swaks \
    --hide-all --to "${TO}" \
    --from "${FROM}" \
    --server "${SMTPSERVER}" \
    --header "Subject: ${SUBJECT} on $(date)" \
    --add-header "MIME-Version: 1.0" \
    --add-header "Content-Type: text/html; charset=utf-8" \
    --body "${TMPFILE}"

rm -f "${TMPFILE}"
rm -f "${CSVFILE}"
