#!/bin/bash
#
#  Simple Backup
#
#  TODO insert a brief explanation here
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

# LVM Volume Groups to activate
VGS=(
    # vg-backup 
    # vg-temporary
)
        
# File systems to mount
MOUNTS=( 
    # /dev/mapper/vg--backup-lv--store	/backup/store 
    # /dev/mapper/vg--backup-lv--backup	/backup/backup 
    # /dev/mapper/vg--temporary-lv_temp	/backup/temporary
    # root@192.168.128.253:/		/backup/altair
    # root@achernar:/			/backup/achernar
    # root@bellatrix:/			/backup/bellatrix
    /dev/disk/by-label/ShandBackup	/backup/storage
)

# Directories to backup, in the following format:
#   source_dir				backup_dir					backup_retention
BACKUPS=(
    # /					/backup/storage/BackUp/tseenfoo/root		1Y
    /etc				/backup/storage/BackUp/tseenfoo/etc		1Y
    /boot				/backup/storage/BackUp/tseenfoo/boot		1Y
    /home/cinpec			/backup/storage/BackUp/tseenfoo/home-cinpec	2Y
    /root				/backup/storage/BackUp/tseenfoo/home-root	2Y
    /srv				/backup/storage/BackUp/tseenfoo/srv		1Y
    /var/lib/pgsql			/backup/storage/BackUp/tseenfoo/pgsql		1Y
    /var/lib/mysql			/backup/storage/BackUp/tseenfoo/mysql		1Y
    /home/store/photo/andcoz		/backup/storage/BackUp/tseenfoo/photo-andcoz	1Y
    /home/store/music/andcoz		/backup/storage/BackUp/tseenfoo/music-andcoz	1Y
    /home/andcoz			/backup/storage/BackUp/tseenfoo/home-andcoz	2Y
    # /backup/altair/etc		/backup/storage/BackUp/altair/etc
    # /backup/altair/boot		/backup/storage/BackUp/altair/boot
    # /backup/altair/srv		/backup/storage/BackUp/altair/srv
    # /backup/altair/var/lib/pgsql	/backup/storage/BackUp/altair/pgsql
    # /backup/altair/var/lib/mysql	/backup/storage/BackUp/altair/mysql
    # /backup/altair/home/andcoz	/backup/storage/BackUp/altair/home-andcoz
    # /backup/achernar/etc		/backup/storage/BackUp/achernar/etc
    # /backup/achernar/srv		/backup/storage/BackUp/achernar/srv
    # /backup/bellatrix/etc		/backup/storage/BackUp/bellatrix/etc
    # /backup/bellatrix/srv		/backup/storage/BackUp/bellatrix/srv
)

# Backups to expire
EXPIRES=(
    # /backup/backup/acrux/etc
    # /backup/backup/acrux/home-andcoz
    # /backup/backup/acrux/home-root
    # /backup/backup/acrux/srv
    # /backup/backup/acrux/pgsql
    # /backup/backup/acrux/mysql
    # /backup/backup/altair/etc
    # /backup/backup/altair/boot
    # /backup/backup/altair/srv
    # /backup/backup/altair/pgsql
    # /backup/backup/altair/mysql
    # /backup/backup/achernar/etc
    # /backup/backup/achernar/srv
    # /backup/backup/bellatrix/etc
    # /backup/backup/bellatrix/srv
)

# Directories to exclude from backups
declare -A EXCLUDES
EXCLUDES[/home/andcoz]=".cache .mozilla/firefox/*/Cache .Wip .mozilla/thunderbird/*.gmail .local/share/Steam/SteamApps"
EXCLUDES[/home/cinpec]=".cache .mozilla/firefox/*/Cache .Wip"
EXCLUDES[/backup/altair/home/andcoz]=".cache .mozilla/firefox/*/Cache .Wip Music"
EXCLUDES[/]="home/andcoz tmp backup run proc var/lib/ntp/proc opt/VMWareVMs opt/temporary opt/VirtualBoxVMs home/store sys"

# Default expire time
EXPIRE_TIME="2Y"

# Additional rdiff-backup flags
#RDIFF_FLAGS="--terminal-verbosity 5"
RDIFF_FLAGS="--exclude-fifos --exclude-device-files --exclude-sockets --no-acls"

# -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- 

BASE=/home/andcoz/Local/share

RDIFF_CMD=rdiff-backup

# export PYTHONPATH=${BASE}/lib/python2.5/site-packages:$PYTHONPATH
# export PATH=${BASE}/bin:$PATH
# export MANPATH=${BASE}/man:${MANPATH}

# -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- 

function do_mount() {
    echo " *** Preparing Disk Storage *** "
    
    local PTR
    local MP
    local MD
    local MOUNTED

    for PTR in ${VGS[@]} ; do
        echo " * activating ${PTR} *"
        
        if ! vgchange -ay ${PTR} ; then
            echo "unable to activate ${PTR}"
            exit -1
        fi
    done
    
    PTR=0
    while [ ${PTR} -lt ${#MOUNTS[@]} ] ; do

       MP="${MOUNTS[${PTR}+1]}"
       MD="${MOUNTS[${PTR}]}"

       RMT=false
       if [ "$(echo ${MD} | tr -d '[a-z0-9./]')" = "@:" ] ; then 
	   RMT=true
       fi

       if ! ${RMT} ; then
	   MD="$(readlink -f ${MD})"
       fi

       echo " * mounting ${MD} *"
        
       if grep -q -E "^[^[:space:]]* ${MP} " /proc/mounts ; then
           MOUNTED=(
               `awk -v mp="${MD}" '$1==mp{print $2}' /proc/mounts`
           )
           
           local FOUND=0
           for RMP in ${MOUNTED[@]} ; do
               if [ "x${RMP}" = "x${MP}" ] ; then
                   FOUND=1
               fi
           done
           
           if [ ${FOUND} -eq 0 ] ; then
               echo "unable to mount ${MD} on ${MP} (already mounted)"
               # exit -2
           fi
       else
	   if ! ${RMT} ; then
               if ! fsck ${MD} ; then 
                   echo "fsck of ${MOUNTS[${PTR}]} failed"
                   exit -1
               fi
               
               if ! mount -o sync ${MD} ${MP} ; then 
                   echo "unable to mount ${MD} on ${MP} (mount failed)"
                   exit -1
               fi
           else
               if ! sshfs ${MD} ${MP} ; then 
                   echo "unable to mount ${MD} on ${MP} (mount failed)"
                   exit -1
               fi       
           fi
       fi
       
       PTR=$(( ${PTR} + 2 ))
    done
}

function do_umount() {
    echo " *** Removing Disk Storage *** "

    local PTR
    local MP
    local MD
    local MOUNTED

    PTR=0
    while [ ${PTR} -lt ${#MOUNTS[@]} ] ; do
        echo " * umounting ${MOUNTS[${PTR}]} *"

        MP="${MOUNTS[${PTR}+1]}"
	MD="${MOUNTS[${PTR}]}"
	
	RMT=false
	if [ "$(echo ${MD} | tr -d '[a-z0-9./]')" = "@:" ] ; then 
	    RMT=true
	fi
	     
	if ! ${RMT} ; then
	    MD="$(readlink -f ${MD})"
	fi

        if [ X`awk -v mp=${MP} -v md=${MD} '($1==md)&&($2==mp){print "M"}' /proc/mounts` == XM ] ; then 
            if ! umount ${MP} ; then
                echo "unable to unmount ${MD} from ${MP} (umounted failed)"     
                exit -2
            fi
        else
            echo "unable to unmount ${MD} from ${MP} (not mounted)"
        fi
        
        PTR=$(( ${PTR} + 2 ))
    done

    for PTR in ${VGS[@]} ; do
        echo " * de-activating ${PTR} *"
        
        if ! vgchange -an ${PTR} ; then
            echo "unable to de-activate ${PTR}"
            exit -1
        fi
    done
}

# -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- [ -+-_== ] -- 

MODE=BACKUP

if [ -n "${1}" ] ; then
    if [ -n "${2}" ] ; then
        MODE=ERROR
    elif [ "X${1}" = X--stats ] ; then 
        MODE=STATS
    elif [ "X${1}" = X--long-stats ] ; then 
        MODE=LONG-STATS
    elif [ "X${1}" = X--help ] ; then 
        MODE=HELP
    elif [ "X${1}" = X--mount ] ; then
        MODE=MOUNT
    elif [ "X${1}" = X--umount ] ; then
        MODE=UMOUNT
    elif [ "X${1}" = X--backup ] ; then
        MODE=BACKUP
    elif [ "X${1}" = X--check ] ; then
        MODE=CHECK
    else 
        MODE=ERROR
    fi
fi

if [ "X${MODE}" = XHELP ] ; then
    echo "backup.sh [--help|--stats|--long-stats|--backup|--mount]"
    exit 0
fi

if [ "X${MODE}" = XERROR ] ; then
    echo "wrong parameter"
    exit -1
fi

if [ "X${MODE}" = XUMOUNT ] ; then
    do_umount
else
#    if [ "X${SSH_AGENT_PID}" = X ] ; then
#        eval `ssh-agent`
#    fi
#
#    ssh-add 

    do_mount
fi 

case ${MODE} in 
    BACKUP)
        echo " *** back-up *** "
        
        PTR=0
        while [ ${PTR} -lt ${#EXPIRES[@]} ] ; do
            MP=${EXPIRES[${PTR}]}
        
            if [ -d ${MP} ] ; then
                echo " * expiring ${MP} increments older than ${EXPIRE_TIME} * "
                if ! ${RDIFF_CMD} ${RDIFF_FLAGS} --force --remove-older-than ${EXPIRE_TIME} ${MP} ; then 
                    echo "unable to clean-up ${MP}"
                    exit -2
                fi
            fi

            ${RDIFF_CMD} -l ${MP}
            
            PTR=$(( ${PTR} + 1 ))
        done
        
        PTR=0
        while [ ${PTR} -lt ${#BACKUPS[@]} ] ; do
            ME=${BACKUPS[${PTR}+2]}
            MP=${BACKUPS[${PTR}+1]}
            MD=${BACKUPS[${PTR}]}

            RDIFF_EXCLUDES=""
            
            EXCLUDE=${EXCLUDES[${MD}]}
            if [ -n "${EXCLUDE}" ] ; then
                echo " * excluding: ${EXCLUDE} * "
                
                OWD=${PWD}
                cd ${MD}
                for i in ${EXCLUDE} ; do
                    RDIFF_EXCLUDES="--exclude ${MD}/${i} ${RDIFF_EXCLUDES}"
                done
                cd ${OWD}
            fi

            if [ -d ${MP} ] ; then
                echo " * expiring ${MP} increments older than ${ME} * "
                if ! ${RDIFF_CMD} ${RDIFF_FLAGS} --force --remove-older-than ${ME} ${MP} ; then 
                    echo "unable to clean-up ${MP}"
                    exit -2
                fi
                
                echo " * backup ${MD} to ${MP} * "
            else
                echo " * first backup to ${MP} *"

                mkdir -p ${MP}
            fi
            
            echo " * executing: ${RDIFF_CMD} ${RDIFF_FLAGS} ${RDIFF_EXCLUDES} ${MD} ${MP} *"
            if ! ${RDIFF_CMD} ${RDIFF_FLAGS} ${RDIFF_EXCLUDES} ${MD} ${MP} ; then 
                 echo "unable to backup ${MD} to ${MP}"
                 exit -1
           fi
            
            ${RDIFF_CMD} -l ${MP}
            
            PTR=$(( ${PTR} + 3 ))
        done
    ;;
        
    STATS)
        echo " *** statistics *** "
        
        PTR=0
        while [ ${PTR} -lt ${#EXPIRES[@]} ] ; do
                echo " * statistics of ${EXPIRES[${PTR}+1]} * "
                ${RDIFF_CMD} -l ${EXPIRES[${PTR}+1]}
                ${RDIFF_CMD} --calculate-average ${EXPIRES[${PTR}+1]}/rdiff-backup-data/session_statistics.*.data
                
                PTR=$(( ${PTR} + 1 ))
        done

        PTR=0
        while [ ${PTR} -lt ${#BACKUPS[@]} ] ; do
                echo " * statistics of ${BACKUPS[${PTR}+1]} * "
                ${RDIFF_CMD} -l ${BACKUPS[${PTR}+1]}
                ${RDIFF_CMD} --calculate-average ${BACKUPS[${PTR}+1]}/rdiff-backup-data/session_statistics.*.data
                
                PTR=$(( ${PTR} + 3 ))
        done
    ;;
        
    LONG-STATS)
        echo " *** statistics *** "
        
        PTR=0
        while [ ${PTR} -lt ${#EXPIRES[@]} ] ; do
                echo " * statistics of ${EXPIRES[${PTR}+1]} * "
                ${RDIFF_CMD} -l ${EXPIRES[${PTR}+1]}
                ${RDIFF_CMD} --list-increment-sizes ${EXPIRES[${PTR}+1]}
                ${RDIFF_CMD} --calculate-average ${EXPIRES[${PTR}+1]}/rdiff-backup-data/session_statistics.*.data
                
                PTR=$(( ${PTR} + 1 ))
        done

        PTR=0
        while [ ${PTR} -lt ${#BACKUPS[@]} ] ; do
                echo " * statistics of ${BACKUPS[${PTR}+1]} * "
                ${RDIFF_CMD} -l ${BACKUPS[${PTR}+1]}
                ${RDIFF_CMD} --list-increment-sizes ${BACKUPS[${PTR}+1]}
                rdiff-backup --calculate-average ${BACKUPS[${PTR}+1]}/rdiff-backup-data/session_statistics.*.data
                
                PTR=$(( ${PTR} + 3 ))
        done
    ;;

    MOUNT|UMOUNT)
        echo " *** Do Nothing *** "

        df -h
    ;;
esac

exit 0;
