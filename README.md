# Simple Shell Tools

A collection of simple tools that makes my days easier.

## [File Handling](file-handling)

* [monitor and move](file-handling/monitornmove.sh): watches a directory for new files and, when they appear, move them to another directory (while quoting Dune)

## [Monitoring](monitoring)

* [psql cron runner](monitoring/psql-cron-runner.sh): template of a simple script that executes some queries on a postgresql server and sends results by email. It requires [swaks](http://www.jetmore.org/john/code/swaks/) to send emails. 

## [VCS Handling](vcs-handling)

* [relocate non java files on svn](vcs-handling/relocate-non-java-files-on-svn.sh): move non `.java` resources to another directory on svn (keeping the directory structure). Helpful when maven-izing some java source code.

## [Application Handling](application-handling)

* [eclipse](application-handling/eclipse.pl): A simple chooser between different eclipse versions.
* [firefox](application-handling/firefox.pl): A simple chooser between different firefox versions and profiles.
