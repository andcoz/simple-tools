#!/bin/sh
#
#  Recursive search for a string on svn
#
#  TODO insert a brief explanation here
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

VERBOSE="false"

# E.g. "--password mypassword"
OPT_ARGS=""

BASE_URL="http://buildenv.maggioli.it/svn/jcitygov"
FILENAME_REGEX='/service.xml$'
FILE_REGEX="julia.?ente"

svn ${OPT_ARGS} list --depth infinity ${BASE_URL} \
    | grep -E "${FILENAME_REGEX}" \
    | while read LINE ; do 
    svn cat ${OPT_ARGS} ${BASE_URL}/${LINE} \
	| grep -q -E -i "${FILE_REGEX}"
    if [ $? -eq 0 ] ; then 
	if [ "${VERBOSE}" == "true" ] ; then 
	    echo -n "    FOUND: "
	fi
        echo "${BASE_URL}/${LINE}"
    else
	if [ "${VERBOSE}" == "true" ] ; then 
            echo "NOT FOUND: ${BASE_URL}/${LINE}"
	fi
    fi
done 
