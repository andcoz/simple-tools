#!/bin/sh
#
#  Relocate non java files on svn
#
#  TODO insert a brief explanation here
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

# rm -rf workarea
# svn co http://someserver.any/svn/someproject workarea
# cd workarea

src=src/main/java
dst=src/main/resources

( cd "$src" ; find . -type f -not -iname \*.java ) | \
while read line ; do 
    d="$(dirname "${line}")"
    p="$(echo ${line} | sed 's,^\./,,')"

    if [ ! -d "${dst}/$d" ] ; then 
	mkdir -p "${dst}/${d}"
	svn add --parents "${dst}/${d}"
    fi

    svn mv "${src}/${p}" "${dst}/${p}"
done
