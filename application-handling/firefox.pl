#!/usr/bin/perl
#
#  firefox launcher
#
#  A simple chooser between different firefox versions and profiles.
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

use strict;

use UI::Dialog;
use Data::Dumper;

my %firefoxes = (
	default => {
		profile     => "default",
		description => 'Personal Navigation',
		command     => "firefox-stable",
		options     => [],
	},
	webdevelop => {
		profile     => "webdevelop",
		description => 'Web Developer Tools',
		command     => "firefox-aurora",
		options     => [],
	},
	demo => {
		profile     => "demo",
		description => 'Clean Configuration for Demo/Presentations',
		command     => "firefox-stable",
		options     => [],
	},
	private => {
		profile     => "private",
		description => 'No history and Privoxy ready',
		command     => "firefox-aurora",
		options     => [],
	},
	nexsoft => {
		profile     => "nexsoft",
		description => 'Customer WiP Configuration',
		command     => "firefox-beta",
		options     => [],
	},
	fastek => {
		profile     => "fastek",
		description => 'Customer WiP Configuration',
		command     => "firefox-beta",
		options     => [],
	},
	maggioli => {
		profile     => "maggioli",
		description => 'Customer WiP Configuration',
		command     => "firefox-stable",
		options     => [],
	},
	cinzia => {
		profile     => "cinzia",
		description => 'Cinzia impersonation',
		command     => "firefox-stable",
		options     => [],
	},
);

# -- -- -------------------------------- ==_-+- --

my $debug = 0;
my @args  = ();

if ( $#ARGV > -1 ) {
	push @args, "firefox-stable";
	push @args, @ARGV;

} else {

	my $d = new UI::Dialog(
		title  => 'Firefox Launcher',
		width  => 600,
		height => 500,
		order  => [ 'zenity', 'gdialog', 'xdialog' ]
	);

	print " D " . Dumper( \%firefoxes ) if $debug;

	my @list = ();
	for my $what ( sort keys %firefoxes ) {
		push @list, $what;
		push @list, $firefoxes{$what}{"description"};
	}

	print " D " . Dumper( \@list ) if $debug;

	my $selection = $d->menu(
		text => 'Select a firefox profile to run: ',
		list => \@list
	);

	print " D " . $selection . "\n" if $debug;

	if ( $selection eq "0" ) {
		exit 0;
	}

	push @args, $firefoxes{$selection}{command}, "-A", $selection,
	  "-P", $selection;
}

print " D " . Dumper( \@args ) if $debug;

exec(@args) == 0
  or die "System @args failed: $?"
