#!/usr/bin/perl
#
#  eclipse launcher
#
#  A simple chooser between different eclipse versions.
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

use strict;

use UI::Dialog;
use Data::Dumper;

my $base = "/home/andcoz/Projects/IDEs";

my %eclipses = (
    spring => {
	description => 'SpringSource Tool Suite [rolling]',
	directory   => "sts-bundle",
	command     => "sts-3.7.0.RELEASE/STS",
	options     => ["-clean"],
    },
    scripting => {
	description => 'Eclipse Juno for Perl and Bash',
	directory   => "eclipse-juno-scripting",
	command     => "./eclipse",
	options     => ["-clean"],
    },
    directory_studio => {
	description => 'Apache Directory Studio 2.0m6',
	directory   => "ApacheDirectoryStudio-linux-x86_64-2.0.0.v20120224",
	command     => "./ApacheDirectoryStudio",
	options     => ["-clean"],
    },
    jee_neon => {
	description => 'Eclipse Neon RC1 for JEE',
	directory   => "eclipse-neon-jee",
	command     => "./eclipse",
	options     => ["-clean"],
    },
    jee_mars => {
	description => 'Eclipse Mars for JEE',
	directory   => "eclipse-mars-jee",
	command     => "./eclipse",
	options     => ["-clean"],
    },
    jee_luna => {
	description => 'Eclipse Luna for JEE',
	directory   => "eclipse-luna-jee",
	command     => "./eclipse",
	options     => ["-clean"],
    },
    jee_kepler => {
	description => 'Eclipse Kepler for JEE',
	directory   => "eclipse-kepler-jee",
	command     => "./eclipse",
	options     => ["-clean"],
    },
    jee_juno => {
	description => 'Eclipse Juno for JEE',
	directory   => "eclipse-juno-jee",
	command     => "./eclipse",
	options     => ["-clean"],
    },
#    liferay_juno => {
#	description => 'Eclipse Juno for LifeRay',
#	directory   => "eclipse-juno-liferay",
#	command     => "./eclipse",
#	options     => ["-clean", "-debug"],
#    },
#    liferay_kepler => {
#	description => 'Eclipse Kepler for LifeRay',
#	directory   => "eclipse-kepler-liferay",
#	command     => "./eclipse",
#	options     => ["-clean", "-debug"],
#    },
#    liferay_luna => {
#	description => 'Eclipse Luna for LifeRay',
#	directory   => "eclipse-luna-liferay",
#	command     => "./eclipse",
#	options     => ["-clean", "-debug"],
#	java        => "jdk-1.7.0",
#    },
    liferay_mars => {
	description => 'Eclipse Mars for LifeRay',
	directory   => "eclipse-mars-liferay",
	command     => "./eclipse",
	options     => ["-clean", "-debug"],
	java        => "jdk-1.8.0",
    },
    bonita => {
	description => 'Bonita Studio 5.9.1',
	directory   => "BOS-5.9.1",
	command     => "./BonitaStudio.sh",
	options     => [],
    },
    cpp => {
	description => 'Eclipse Juno SR2 for cpp',
	directory   => "eclipse-juno-sr2-cpp",
	command     => "./eclipse",
	options     => ["-clean"],
    },
);

# -- -- -------------------------------- ==_-+- --

my $debug = 0;

my $d = new UI::Dialog(
    title  => 'Eclipse Launcher',
    width  => 600,
    height => 500,
    order  => [ 'yad', 'zenity', 'gdialog', 'xdialog' ]
    );
 
print " D " . Dumper( \%eclipses ) if $debug;

my @list = ();
for my $what ( sort keys %eclipses ) {
    push @list, $what;
    push @list, $eclipses{$what}{"description"};
}

print " D " . Dumper( \@list ) if $debug;

my $selection = $d->menu(
    text => 'Select an eclipse installation to run: ',
    list => \@list
    );

print " D " . $selection . "\n" if $debug;

if ( $selection eq "0" ) {
    exit 0;
}

print " D " . $base . "/" . $eclipses{$selection}{"directory"} . "\n" if $debug;

chdir( $base . "/" . $eclipses{$selection}{"directory"} )
    or die "Can't cd to " . $eclipses{$selection}{"directory"} . ": $!\n";

my @args = ( $eclipses{$selection}{command}, @{ $eclipses{$selection}{options} } );

print " D " . Dumper( \@args ) if $debug;

exec(@args) == 0
    or die "System @args failed: $?"
