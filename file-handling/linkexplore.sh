#!/bin/bash
#
#  Link Explore
#
#  TODO insert a brief explanation here
#
#  Copyright  2021, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

function usage {
    echo "${basename}: "
}

basename="$(basename ${0})"
target="${1}"

type=$(stat --printf="%F" "${target}")

while
    [[ -e "${target}" ]] && [[ "${type}" == "symbolic link" ]] ;
do
    stat --format="%N" "${target}"
    
    type=$(stat --printf="%F" "${target}")
    target=$(stat --printf="%N" "${target}" | sed "s,.*-> ,,;s,',,g")
done
