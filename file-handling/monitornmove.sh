#!/bin/bash 
#
#  Monitor and Move 
#
#  TODO insert a brief explanation here
#
#  Copyright  2010-2015, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

usage () {
    echo ""
    echo "Usage: $(basename $0) [-c COUNTER] [-n] \\"
    echo "          PATTERN DESTINATION"
    echo "Usage: $(basename $0) -h"
    echo ""
    
    # TODO Inserire qui la descrizione dei parametri
}

COUNTER=1
HASTOCOUNT=false

while getopts ":nc:h" Option ; do
  case $Option in
    c  ) COUNTER=${OPTARG} ;;
    n  ) HASTOCOUNT=true ;;
    h  ) usage ; exit 0 ;;
    \? ) echo "$(basename $0): unknown -${OPTARG} option" 
         usage ; exit 1 ;; 
    :  ) echo "$(basename $0): missing parameter to -${OPTARG} option" 
         usage ; exit 1 ;; 
  esac
done

shift $(( ${OPTIND} - 1 ))

if [ -z "$2" ] ; then 
    usage ;
    exit -1 ; 
fi

SRC_PATTERN="$1"
DST_DIR="$2"

clear
echo ""
echo "The sleeper must awaken"
echo "(c) Frank Herbert, 1965"
echo ""
echo -e "monitoring:\t${SRC_PATTERN}"
echo -e "destination:\t${DST_DIR}"

if [ ${HASTOCOUNT} == 'true' ] ; then
   echo -e "rename from\t${COUNTER}"
fi

echo ""


while (true); do 
    tmpfile=$( mktemp )
    found=0
    filelist=

    ls -1 ${SRC_PATTERN} > "${tmpfile}" 2> /dev/null

    while read line; do
        if [ ${HASTOCOUNT} == 'true' ] ; then
	    filename=$(basename "$line")
	    extension="${filename##*.}"

	    dst="${DST_DIR}/$(printf "%04d" ${COUNTER}).${extension}"

	    COUNTER=$((COUNTER +1))
	else
	    dst="${DST_DIR}"
	fi
	
	mv "${line}" "${dst}"
	found=$(( found + 1 ))
	filelist="$filelist $(basename "${line}")"
    done < "$tmpfile"

    rm -f "$tmpfile"

    if [ $found -eq 0 ] ; then
	RND=$(( RANDOM % 20 ))
	case "$RND" in 
	    ?0|?5|?7)
		echo -n "Z" 
		;;
	    13)
		echo -n " (snort) "
		;;
	    *)
		echo -n "z" 
		;;
	esac
    else
	echo "zzz ..."
	echo "Father ... father, the sleeper has awakened!"
	echo " * ${found} file moved (${filelist} )"
    fi

    sleep 3
done

exit

