#!/bin/bash 
#
#  Photo Selector
#
#  TODO insert a brief explanation here
#
#  Copyright  2018, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU 
#  General Public License. You may redistribute and/or modify this 
#  program under the terms of the version 2 of that license as 
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#


FILES=(*.jpg)
TDIR=$(mktemp -d -t)

convert -size 1000x800 xc:green ${TDIR}/bgc.bmp
convert -size 600x400  xc:red   ${TDIR}/bgp.bmp
convert -size 600x400  xc:blue  ${TDIR}/bgn.bmp
convert -size 1600x800 xc:white ${TDIR}/bg.bmp

SIZE=${#FILES[*]}

for (( I=0; I<SIZE; I++ )) do 

    echo "processing: ${I}/${SIZE} ==> ${FILES[I]}"

    convert -resize 1000x800 "${FILES[I]}" ${TDIR}/imc.bmp
    composite -gravity center ${TDIR}/imc.bmp ${TDIR}/bgc.bmp ${TDIR}/ibc.bmp

    if (( $I == 0 )) ; then
        cp ${TDIR}/bgp.bmp ${TDIR}/ibp.bmp
    else
        if [ -f "${FILES[I-1]}" ] ; then
            convert -resize 600x400 "${FILES[I-1]}" ${TDIR}/imp.bmp
            composite -gravity center ${TDIR}/imp.bmp ${TDIR}/bgp.bmp ${TDIR}/ibp.bmp
        fi
    fi

    if (( $I == ${SIZE}-1 )) ; then
        cp ${TDIR}/bgn.bmp ${TDIR}/ibn.bmp
    else
        convert -resize 600x400 "${FILES[I+1]}" ${TDIR}/imn.bmp
        composite -gravity center ${TDIR}/imn.bmp ${TDIR}/bgn.bmp ${TDIR}/ibn.bmp
    fi

    convert \
        -size 1600x800  ${TDIR}/bg.bmp  \
        -page +0+0      ${TDIR}/ibc.bmp \
        -page +1000+0   ${TDIR}/ibp.bmp \
        -page +1000+400 ${TDIR}/ibn.bmp \
        -layers flatten ${TDIR}/image.bmp

    yad --picture \
        --filename ${TDIR}/image.bmp \
        --title="Select? ${I} on ${SIZE}" \
        --height=850 --width=1650 --size=orig \
        --button=Keep:1 --button=Delete:2 
    
    if [ $? -eq 2 ] ; then 
        rm  "${FILES[I]}"
    fi

done

rm -rf $TDIR
